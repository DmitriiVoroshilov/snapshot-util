from setuptools import setup

setup(
    name='snapshot',
    version='1.0',
    author="Dmitrii Voroshilov",
    description='Simple python app which would monitor your system/server. Output should be written to json file and stdout.',
    install_requires=['psutil'],
    packages=['snapshot'],
)
