"""
Make snapshot

{"Tasks": {"total": 440, "running": 1, "sleeping": 354, "stopped": 1, "zombie": 0},
"%CPU": {"user": 14.4, "system": 2.2, "idle": 82.7},
"KiB Mem": {"total": 16280636, "free": 335140, "used": 11621308},
"KiB Swap": {"total": 16280636, "free": 335140, "used": 11621308},
"Timestamp": 1624400255}
"""
import argparse
from datetime import datetime
import json
import os
import psutil
import time


class ProcessesInf:

    def __init__(self, proc_status):
        self.proc_status = proc_status

    def proc_num(self):
        return_proc = {'Tasks': {}}
        for stat in self.proc_status:
            if stat == 'total':
                proc_running = len(list(psutil.process_iter()))
            else:
                def get_proc(total):
                    return filter(
                        lambda proc: proc.status() == total, psutil.process_iter())
                proc_running = len(list(get_proc(stat)))
            return_proc['Tasks'][stat] = proc_running
        return return_proc


class CpuInf:

    def __init__(self, cpu_stat):
        self.cpu_stat = cpu_stat

    def cpu_time(self):
        return_cpu = {'%CPU': {}}
        a = psutil.cpu_times_percent()
        for ct in self.cpu_stat:
            b = getattr(a, ct)
            return_cpu['%CPU'][ct] = b
        return return_cpu


class MemInf:

    def __init__(self, mem_stat):
        self.mem_stat = mem_stat

    def mem_size(self):
        return_mem = {'KiB Mem': {}}
        a = psutil.virtual_memory()
        for ms in self.mem_stat:
            b = int(getattr(a, ms) / 1024)
            return_mem['KiB Mem'][ms] = b
        return return_mem


class SwapInf:

    def __init__(self, swap_stat):
        self.swap_stat = swap_stat

    def swap_size(self):
        return_swap = {'KiB Swap': {}}
        a = psutil.swap_memory()
        for sm in self.swap_stat:
            b = int(getattr(a, sm) / 1024)
            return_swap['KiB Swap'][sm] = b
        return return_swap


def main():
    """Snapshot tool."""
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', help='Interval between snapshots in seconds', type=int, default=30)
    parser.add_argument('-f', help='Output file name', default='snapshot.json')
    parser.add_argument('-n', help='Quantity of snapshot to output', type=int, default=3)
    args = parser.parse_args()

    get_proc = ProcessesInf(['total', 'running', 'sleeping', 'stopped', 'zombie'])
    get_cpu = CpuInf(['user', 'system', 'idle'])
    get_mem = MemInf(['total', 'free', 'used'])
    get_swap = SwapInf(['total', 'free', 'used'])

    check_file = os.path.exists(args.f)

    if check_file:

        with open(args.f, 'w') as file:
            file.close

    for _ in range(0, args.n):

        dt = datetime.now()
        ts = {'Timestamp': int(datetime.timestamp(dt))}

        snapshot = {**get_proc.proc_num(), **get_cpu.cpu_time(), **get_mem.mem_size(),
                    **get_swap.swap_size(), **ts}

        print(snapshot, end="\r")

        with open(args.f, 'a') as file:
            json.dump(snapshot, file)
            file.write("\n")

        time.sleep(args.i)


if __name__ == '__main__':
    main()
 